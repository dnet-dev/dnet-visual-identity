# Decenternet's visual identity

Includes:

* Logo
* Isotype
* Fonts
  * Logo: Lombok
  * Headings: Rajdhani
  * Text body: Roboto Light
* Colour Palette

## Credits

* "Lombok" Typeface by Alexandre Pietra (https://www.behance.net/AlexandrePietra)
  * (source: https://github.com/gmkhussain/fonts/blob/master/FONTS/Lombok.otf)
* "Rajdhani" Typeface by Christian Robertson (https://fonts.google.com/specimen/Rajdhani)
* "Roboto" Typeface by Indian Type Foundry (https://fonts.google.com/specimen/Roboto)
